import Vue from 'vue';
import Vuex from 'vuex';
import Fuse from 'fuse.js';

import * as services from '@/services';


Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    ParkList: [],
    ParkSearchList: [],
    ParkRecommendList: [],
  },

  mutations: {
    SetParkList(state, parkList) {
      state.ParkList = parkList;
    },

    SetParkSearchList(state, parkList) {
      state.ParkSearchList = parkList;
    },

    SetParkRecommendList(state, parkList) {
      state.ParkRecommendList = parkList;
    },
  },

  actions: {
    async LoadParkList({ commit }) {
      const parkList = await services.getParkList();
      const {
        loads,
      } = await services.getParkDetailsList({
        dateStart: new Date(),
        dateEnd: new Date(),
      });

      const parkRecommendList = parkList
        .map(park => ({
          ...park,
          load: loads[park.park_id],
        }));

      commit('SetParkList', parkList);
      commit('SetParkRecommendList', parkRecommendList);
    },

    async SearchParkList({ commit, state }, { searchText, dateStart, dateEnd }) {
      const {
        loads,
        recommended,
      } = await services.getParkDetailsList({
        dateStart,
        dateEnd,
      });

      const parkSearchList = new Fuse(
        state.ParkList,
        {
          keys: [{
            name: 'park_name',
            weight: 0.7,
          }, {
            name: 'description',
            weight: 0.3,
          }],
        },
      )
        .search(searchText)
        .map(park => ({
          ...park,
          load: loads[park.park_id],
        }));

      const parkRecommendList = recommended
        .map((parkId) => {
          const park = state.ParkList.find(it => it.park_id === parkId);
          return {
            ...park,
            load: loads[parkId],
          };
        });

      commit('SetParkSearchList', searchText ? parkSearchList : []);
      commit('SetParkRecommendList', parkRecommendList);
    },
  },
});
