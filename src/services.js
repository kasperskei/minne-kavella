import wretch from 'wretch';


const request = wretch(`${process.env.VUE_APP_API_URL}/api`)
  .options({
    // mode: 'no-cors',
    // credentials: 'include',
    // referrer: 'no-referrer',
    // headers: {
    //   Accept: 'application/json',
    // },
  });


/**
 * description: String
 * lat: Number
 * link: String
 * lon: Number
 * park_id: Number
 * park_name: String
 * tags: String[]
 * thumb: String
 */
export const getParkList = () => request
  .url('/get_static_info')
  .get()
  .json();

export const getParkDetailsList = ({ dateStart, dateEnd }) => request
  .url('/get_recommendations')
  .query({
    ts_start: +dateStart,
    ts_end: +dateEnd,
  })
  .get()
  .json();

export const getPark = ({ parkId, dateStart, dateEnd }) => (console.log(dateStart, dateEnd), request
  .url('/get_full_info')
  .query({
    park_id: parkId,
    ts_start: +dateStart,
    ts_end: +dateEnd,
  })
  .get()
  .json());
